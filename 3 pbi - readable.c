#include <unistd.h>

#define STDIN 0
#define STDERR 2

char data[9999];
char *bracketPositions[99];
char *instructionPointer = data;
char *dataPointer = data + 5000;
char **currentBracket = bracketPositions;
char bracketFlag;
char command;

int main() {
    read(STDIN, instructionPointer, 4000);

    command = *instructionPointer;

    while (command != 0) {
        if (command == ']') {
            if (bracketFlag <= 1) {
                if (*dataPointer != 0) {
                    instructionPointer = *currentBracket;
                } else {
                    currentBracket--;
                }
            }
            if (bracketFlag != 0) {
                bracketFlag--;
            }
        }

        if (command == '[') {
            bracketFlag++;
            if (bracketFlag == 1) {
                currentBracket++;
                *currentBracket = instructionPointer;
            }
        }

        if (bracketFlag == 0) {
            switch (command) {
                case '+':
                    *dataPointer += 1;
                    break;
                case '-':
                    *dataPointer -= 1;
                    break;
                case '>':
                    dataPointer += 1;
                    break;
                case '<':
                    dataPointer -= 1;
                    break;
                case '.':
                    write(STDERR, dataPointer, 1);
                    break;
                case ',':
                    read(STDERR, dataPointer, 1);
                    break;
            }
        }

        instructionPointer++;
        command = *instructionPointer;
    }
}
