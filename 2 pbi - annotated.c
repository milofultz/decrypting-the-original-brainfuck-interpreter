#define STDIN 0
#define STDERR 2

// set upper limit of all data values to be 255
char data[9999];
char *bracketPositions[99];
char *instructionPointer = data;
char *dataPointer = data + 5000;
char **currentBracket = bracketPositions;
char bracketFlag;
char command;

int main() {
    // For each character read from STDIN
    for (
        // Read up to 4000 commands into the first 400 slots of data
        read(STDIN, instructionPointer, 4000);
        // Set command to the value at instructionPointer for every loop and
        // check that command is not 0, else end the loop
        (command = *instructionPointer);
        // Increment instructionPointer on every loop
        instructionPointer++
    ) {
        // if command == ']',
        command - ']' || (
            // if bracketFlag <= 1,
            bracketFlag > 1 ||
                // if value at dataPointer is not zero,
                (instructionPointer = *dataPointer
                    // set instructionPointer to value at pointer currentBracket
                    ? *currentBracket
                    // else, decrement currentBracket (and keep
                    // instructionPointer as instructionPointer)
                    : (--currentBracket, instructionPointer)
                ),
            // if bracketFlag is not 0, decrement bracketFlag
            !bracketFlag || bracketFlag--
        ),
        // if command == '[',
        command - '[' ||
            // increment bracketFlag. if bracketFlag pre-increment is zero,
            bracketFlag++ ||
                // increment currentBracket and set value at currentBracket to
                // the instructionPointer
                (*++currentBracket = instructionPointer),
        // if bracketFlag is zero,
        bracketFlag || (
            // Because value at dataPointer is a char, value will wrap around if
            // it reaches below 0 or above 255

            // Increment value at dataPointer if command is '+'
            *dataPointer += command == '+',
            // Decrement value at dataPointer if command is '-'
            *dataPointer -= command == '-',

            // Increment dataPointer if command is '>'
            dataPointer += command == '>',
            // Decrement dataPointer if command is '<'
            dataPointer -= command == '<',

            // If command is '.', write 1 command at datapointer to STDERR
            command - '.' || write(STDERR, dataPointer, 1),
            // If command is ',', read 1 command to datapointer from STDERR
            command - ',' || read(STDERR, dataPointer, 1)
        );
    }
}
