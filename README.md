When I found the Portable Brainfuck Interpreter by Urban Mueller, who also created the language, it was on [Linus Akesson's](http://www.linusakesson.net/programming/brainfuck/index.php) webpage, totally crushed and minimized. I had just finished writing [my own BF interpreter](https://codeberg.org/milofultz/brainfuck), which was long and very verbose, so I wanted to know how much slimmer this portable version of the interpreter really was.

So what I did here was:

* Expand the original to where it was more readable
* Annotate what was happening in plain language
* Refactor it to make it easier to follow, esp for a C rube like me

I learned a LOT about operator precedence (particular postfix and prefix notation), comma operators, file descriptors, and a few other things. Overall a fun knot to unravel.

